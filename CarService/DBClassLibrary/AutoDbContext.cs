﻿using DBClassLibrary.Model;
using Microsoft.EntityFrameworkCore;

namespace DBClassLibrary
{
    public class AutoDbContext : DbContext
    {
        public AutoDbContext(DbContextOptions options) : base(options)
        {

        }
        public DbSet<User> Users { get; set; }
        public DbSet<Tasks> Tasks { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Car> Cars { get; set; }
        public DbSet<TaskPriority> TaskPriorities { get; set; }

        public AutoDbContext()
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // Loggin into debug-log
            optionsBuilder.LogTo(message => System.Diagnostics.Debug.WriteLine(message));
        }
       
    }
}
