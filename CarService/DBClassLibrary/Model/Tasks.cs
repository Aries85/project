﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBClassLibrary.Model
{
    [Index(nameof(Title), IsUnique = true)]
    public class Tasks
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [Required]
        public int Id { get; set; }
        public int ParentID { get; set; }
        [Required]
        public int TaskPrioritID { get; set; }
        [Required]
        [MaxLength(50)]
        public string Title { get; set; }
        [Column(TypeName = "text")]
        public string Deskription { get; set; }
        [Required]
        public bool IsCompleted { get; set; }
        public bool IsInWork { get; set; }
        public bool IsPending { get; set; }


    }
}
