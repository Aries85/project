﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBClassLibrary.Model
{
    public class TaskPriority
    {
        [Key]
        [Required]
        public int ID { get; set; }
        [Required]
        public string Titel { get; set; }
        
    }
}
