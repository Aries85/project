﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBClassLibrary.Model
{
    [Index(nameof(VIN), IsUnique = true)]
    public class Car
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Model { get; set; }
        [MaxLength(50)]
        public string Manufak { get; set; }
        [Required]
        public string VIN { get; set; }
        [Required]
        public int Number { get; set; }
        [Required]
        public int ClientID { get; set; }
    }
}
