﻿using System;
using DBClassLibrary;
using Microsoft.Extensions.Configuration;
using System.IO;
using Microsoft.EntityFrameworkCore;
using DBClassLibrary.Model;
using System.Linq;

namespace ConsoleApp1
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder();
            // установка пути к текущему каталогу
            builder.SetBasePath(Directory.GetCurrentDirectory());
            // получаем конфигурацию из файла appsettings.json
            builder.AddJsonFile("appsettings.json");
            // создаем конфигурацию
            var config = builder.Build();
            // получаем строку подключения
            string connectionString = config.GetConnectionString("DefaultConnection");

            var optionsBuilder = new DbContextOptionsBuilder<AutoDbContext>();
            var options = optionsBuilder.UseSqlServer(connectionString).Options;

            using (AutoDbContext db = new AutoDbContext(options))
            {
                TaskPriority priority1 = new TaskPriority { Titel = "Major" };
                TaskPriority priority2 = new TaskPriority { Titel = "Minor" };
                TaskPriority priority3 = new TaskPriority { Titel = "Critical" };

                db.TaskPriorities.Add(priority1);
                db.TaskPriorities.Add(priority2);
                db.TaskPriorities.Add(priority3);
                db.SaveChanges();
            }

        }
    }
}
