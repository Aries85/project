﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DbProjectLibrary.Model
{
    public class Defects
    {
        public int Id { get; set; }
        public int RegistrationDate { get; set; }
        public string RailroadThread { get; set; }
        public string TypeOfDefect { get; set; }
        public string TypeOfRail { get; set; }
        public string DeskriptionDefect { get; set; }
        public bool IsCompleted { get; set; }
        public bool IsInWork { get; set; }
        public bool IsPending { get; set; }
        public int PicketID { get; set; }
    }
}
