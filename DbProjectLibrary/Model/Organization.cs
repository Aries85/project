﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DbProjectLibrary.Model
{
    public class Organization
    {
        public int OrganizationID { get; set; }
        public string OrganizationName { get; set; }
        public string SubOrganizationName { get; set; }
        //public int SubOrganizationID { get; set; }
        public int UserID { get; set; }
        
    }
}
