﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DbProjectLibrary.Model
{
    public class RailwaySections
    {
        public int RailwaySectionsId { get; set; }
        public int OrganizationID { get; set; }
        public string SectionName { get; set; }
        public int TrackNumber { get; set; }
        public int LocalSectionId { get; set; }

    }
}
