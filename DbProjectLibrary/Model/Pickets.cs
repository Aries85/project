﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DbProjectLibrary.Model
{
    public class Pickets
    {
        public int PicketsID { get; set; }
        public int KilometrID { get; set; }
        public int NumberOfPicket { get; set; }
    }
}
