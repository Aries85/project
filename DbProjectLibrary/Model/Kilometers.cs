﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DbProjectLibrary.Model
{
    public class Kilometers
    {
        public int KilometersID { get; set; }
        public int NumberOfKilometrs { get; set; }
        public int SectionID { get; set; }
    }
}
