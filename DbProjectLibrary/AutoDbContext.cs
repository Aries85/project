﻿using DbProjectLibrary.Model;
using Microsoft.EntityFrameworkCore;


namespace DbProjectLibrary
{
    public class AutoDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<RailwaySections> RailwaySections { get; set; }
        public DbSet<Kilometers> Kilometers { get; set; }
        public DbSet<Pickets> Pickets { get; set; }
        public DbSet<Defects> Defects { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=SECURITY1\\SQLEXPRESS;Initial Catalog=DatabaseProject;Integrated Security=True");
        }
    }
}
